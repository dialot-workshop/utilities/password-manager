import boto3
from boto3.dynamodb.conditions import Key
import os

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context):
    identifier = os.environ['identifier']
    environment = os.environ['environment']

    customer_id = event['customerId']
    password_key = event['passwordKey']

    # validate if this user can retrieve this password
    prefix = identifier + '-' + environment + '-'
    if not validate_user_key(customer_id, event['headers']['x-user-key'], prefix):
        return {'statusCode': 401}

    # fetch the password
    table = dynamodb.Table(prefix + 'PasswordManager')
    response = table.query(
        IndexName='CustomerId-passwordKey-index',
        KeyConditionExpression=Key('CustomerId').eq(customer_id)
    )

    list_password_key = []
    for item in response['Items']:
        if not is_search_required(password_key) or password_key in item['passwordKey']:
            list_password_key.append(item['passwordKey'])

    response = {
        'statusCode': 200,
        'body': {
            'passwordKeys': list_password_key
        },
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }

    return response


def is_search_required(password_key):
    """

    """
    return password_key != ""


def validate_user_key(username, user_key, table_prefix):
    """
    Validate if the given username/key pair is valide
    """
    user_table = dynamodb.Table(table_prefix + 'PasswordManagerUserKey')
    user_data = user_table.get_item(
        Key={
            'Username': username
        })

    return user_data['Item']['key'] == user_key
