from cryptography.fernet import Fernet
import boto3
import os

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context):
    identifier = os.environ['identifier']
    environment = os.environ['environment']

    username = event['username']
    password_key = event['passwordKey']

    # validate if this user can retrieve this password
    prefix = identifier + '-' + environment + '-'
    if not validate_user_key(username, event['headers']['x-user-key'], prefix):
        return {'statusCode': 401}

    # fetch the password
    table = dynamodb.Table(prefix + 'PasswordManager')
    data = table.get_item(
        Key={
            'UserPasswordKey': username + "#" + password_key,
            'CustomerId': username
        })

    # decrypt it
    decrypted_password = decrypt_password(str.encode(data['Item']['key']), str.encode(data['Item']['password']))

    # send it
    response = {
        'statusCode': 200,
        'body': {
            'username': data['Item']['username'],
            'password': decrypted_password
        },
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }

    return response


def validate_user_key(username, user_key, table_prefix):
    """
    Validate if the given username/key pair is valide
    """
    user_table = dynamodb.Table(table_prefix + 'PasswordManagerUserKey')
    user_data = user_table.get_item(
        Key={
            'Username': username
        })

    return user_data['Item']['key'] == user_key


def decrypt_password(encryption_key, encrypted_password):
    """
    Decrypted the given encrypted password with the given encryption key
    """
    return Fernet(encryption_key).decrypt(encrypted_password).decode()
