import boto3
import os
from cryptography.fernet import Fernet

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context):
    identifier = os.environ['identifier']
    environment = os.environ['environment']

    customer_id = event['customerId']
    username = event['username']
    password_key = event['passwordKey']
    password = event['password']

    key = Fernet.generate_key()
    fernet = Fernet(key)
    enc_password = fernet.encrypt(password.encode())

    table = dynamodb.Table(identifier + '-' + environment + '-' + 'PasswordManager')
    data = table.put_item(
        Item={
            'UserPasswordKey': customer_id + "#" + password_key,
            'CustomerId': customer_id,
            'username': username,
            'password': str(enc_password, 'UTF-8'),
            'passwordKey': password_key,
            'key': str(key, 'UTF-8')
        }
    )

    response = {
        'statusCode': 201,
        'body': customer_id + "#" + password_key,
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }

    return response
