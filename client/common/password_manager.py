import requests
import yaml


class PasswordManager:
    """Main class containing the logic to call the PasswordManager API and returned the asked information.
    """

    SETTINGS_FILES_PATH = "../settings/local-env.yaml"

    def __init__(self, customer_id):
        self.customer_id = customer_id

        with open(self.SETTINGS_FILES_PATH, "r") as stream:
            try:
                self.yaml_config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def list_password_key(self, password_key):
        """Return an array of password keys that a user can then execute a search on.
        """

        uri = self.yaml_config['api-endpoint'] + "/passwords" + "?customerId=" + self.customer_id
        if password_key != "":
            uri += "&passwordKey=" + password_key

        req = requests.get(
            uri,
            headers={
                "x-api-key": self.yaml_config['api-key'],
                "x-user-key": self.yaml_config['user-key']
            })

        return req.json()['body']['passwordKeys']

    def post_password(self, username, password_key, password):
        """Store a new password for the given username/key pair.
        """

        body = {"customerId": self.customer_id, "username": username, "passwordKey": password_key, "password": password}

        req = requests.post(
            self.yaml_config['api-endpoint'],
            headers={
                "x-api-key": self.yaml_config['api-key'],
                "x-user-key": self.yaml_config['user-key']
            },
            json=body)

        if req.status_code == 201:
            print("Password stored successfully")

        return req.json()

    def retrieve_password_from_user(self, password_key):
        """Get the related password information from the given password key.
           This method will return an instance of the object RetrievedPasswordInfo.
        """
        req = requests.get(
            self.yaml_config['api-endpoint'] + "?username=" + self.customer_id + "&passwordKey=" + password_key,
            headers={"x-api-key": self.yaml_config['api-key'], "x-user-key": self.yaml_config['user-key']})

        return RetrievedPasswordInfo(req.json()['body']['username'], req.json()['body']['password'])


class RetrievedPasswordInfo:
    """Simple DTO containing the information related to a password.
    """

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password
