import argparse
import sys

sys.path.append("../..")  # Workaround in order to import for sibling module
from client.common.password_manager import PasswordManager


def parse_arguments():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Positional mandatory arguments
    parser.add_argument("customerId", help="Identifier of the Customer", type=str)
    parser.add_argument("username", help="Username related to this password", type=str)
    parser.add_argument("passwordKey", help="Password Key related to this password", type=str)
    parser.add_argument("password", help="Password to be stored", type=str)

    # Print version
    parser.add_argument("-v", "--version", action="version", version='%(prog)s - Version 1.0')

    return parser.parse_args()


if __name__ == '__main__':
    """
    Store a password given in the parameters
    """
    args = parse_arguments()

    customer_id = args.customerId
    username = args.username
    password_key = args.passwordKey
    password = args.password

    password_manager = PasswordManager(args.customerId)

    print(password_manager.post_password(username, password_key, password))
