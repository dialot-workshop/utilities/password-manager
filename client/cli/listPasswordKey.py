import argparse
import sys

sys.path.append("../..")  # Workaround in order to import for sibling module
from client.common.password_manager import PasswordManager


def parse_arguments():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Positional mandatory arguments
    parser.add_argument("customerId", help="Identifier of the Customer", type=str)

    # Optional arguments
    parser.add_argument("-p", "--pwdKey", help="Password Key that you are looking for", type=str, default="")

    # Print version
    parser.add_argument("-v", "--version", action="version", version='%(prog)s - Version 1.0')

    return parser.parse_args()


if __name__ == '__main__':
    """
    Call the "search" rest endpoint for the Password Manager API.
    """
    args = parse_arguments()

    password_manager = PasswordManager(args.customerId)

    print(sorted(list(password_manager.list_password_key(args.pwdKey)), key=str.lower))
