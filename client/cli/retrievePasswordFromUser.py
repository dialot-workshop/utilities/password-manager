import argparse
import sys

sys.path.append("../..")  # Workaround in order to import for sibling module
from client.common.password_manager import PasswordManager


def parse_arguments():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Positional mandatory arguments
    parser.add_argument("customerId", help="Identifier of the Customer", type=str)
    parser.add_argument("passwordKey", help="Username related to this password", type=str)

    # Print version
    parser.add_argument("-v", "--version", action="version", version='%(prog)s - Version 1.0')

    return parser.parse_args()


if __name__ == '__main__':
    """
    Store a password given in the parameters
    """
    args = parse_arguments()

    customerId = args.customerId
    password_key = args.passwordKey

    password_manager = PasswordManager(args.customerId)

    retrieved_password_info = password_manager.retrieve_password_from_user(password_key)

    print(retrieved_password_info.get_username())
    print(retrieved_password_info.get_password())
