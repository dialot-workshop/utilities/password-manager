import PySimpleGUIWx as sg
import yaml
from password_client_printer import PasswordClientPrinter


with open("../settings/local-env.yaml", "r") as stream:
    try:
        yaml_config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

password_client_printer = PasswordClientPrinter(yaml_config['customer-id'])

# Define the window's contents
layout = [[sg.Text("What's the password you are looking for ?")],
          [sg.Input(key='-INPUT-')],
          [sg.Output(key='-OUTPUT-', size=(80, 20))],
          [sg.Button('Get List'), sg.Button('Get Password'), sg.Button('Quit')]]

# Create the window
window = sg.Window('Password Manager', layout)

# Display and interact with the Window using an Event Loop
while True:
    event, values = window.read()
    # See if user wants to quit or window was closed
    if event == sg.WINDOW_CLOSED or event == 'Quit':
        break
    if event == 'Get Password':
        password_client_printer.print_get_password(values['-INPUT-'])
    if event == 'Get List':
        password_client_printer.print_list_password(values['-INPUT-'])

# Finish up by removing from the screen
window.close()
