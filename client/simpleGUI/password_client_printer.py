import sys
sys.path.append("../..")  # Workaround in order to import for sibling module
from client.common.password_manager import PasswordManager


class PasswordClientPrinter:
    """A printer used to display information related to the PasswordManager class into the std output console.
    """

    def __init__(self, customer_id):
        self.password_manager = PasswordManager(customer_id)

    def print_list_password(self, password_key):
        """Will print all the password in relation of the given password_key sorted in alphabetical order.
        """
        print(sorted(list(self.password_manager.list_password_key(password_key)), key=str.lower))

    def print_get_password(self, password_key):
        """Wil print the username/password pair related to the given password_key.
        """
        retrieved_password_info = self.password_manager.retrieve_password_from_user(password_key)
        print("Username for " + password_key + " is : " + retrieved_password_info.get_username())
        print("Password for " + password_key + " is : " + retrieved_password_info.get_password())

