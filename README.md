# Custom AWS Hosted Password Manager

Project use to create a custom password manager service in an AWS account

After creating the required infrastructure with the given terraform plan, you will be able  to reach different 
API endpoint in order to store/retrieve passwords.

## CLI
First step is to create a local-env.yaml file with the configuration needed in order to communicate with the API.

Needed properties are : 
- api-endpoint
- api-key
- user-key

Then you can use the script available under the /client/cli folder.

## Architecture
![](./aws-diagram.png)

## Local development
In order to validate some terraform change locally, you can use the following command
```
terraform apply -var-file="dev.tfvars" --auto-approve
```

## Testing
Example of using the CLI

```
python3 .\postPassword.py customerId myUsername passwordKey mySecurePassword
```

```
python3 .\retrievePasswordFromUser.py customerId passwordKey
```

