resource "aws_dynamodb_table" "password_manager" {
  name           = "${var.identifier}-${var.environment}-PasswordManager"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "UserPasswordKey"
  range_key      = "CustomerId"

  attribute {
    name = "UserPasswordKey"
    type = "S"
  }

  attribute {
    name = "CustomerId"
    type = "S"
  }

    attribute {
    name = "passwordKey"
    type = "S"
  }

  global_secondary_index {
    name               = "CustomerId-passwordKey-index"
    hash_key           = "CustomerId"
    range_key          = "passwordKey"
    projection_type    = "ALL"
  }

}

resource "aws_dynamodb_table" "password_manager_user_key" {
  name           = "${var.identifier}-${var.environment}-PasswordManagerUserKey"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "Username"

  attribute {
    name = "Username"
    type = "S"
  }

}