resource "aws_api_gateway_rest_api" "password_manager" {
  name        = "${var.identifier}-${var.environment}-PasswordManager"
  description = "API for the password Manager Service"
}

resource "aws_api_gateway_resource" "root" {
  rest_api_id = aws_api_gateway_rest_api.password_manager.id
  parent_id   = aws_api_gateway_rest_api.password_manager.root_resource_id
  path_part   = "passwordManager"
}

resource "aws_api_gateway_method" "get_root" {
  authorization     = "NONE"
  http_method       = "GET"
  api_key_required  = true
  resource_id       = aws_api_gateway_resource.root.id
  rest_api_id       = aws_api_gateway_rest_api.password_manager.id
}

resource "aws_api_gateway_integration" "get_password_lambda" {
  rest_api_id             = aws_api_gateway_rest_api.password_manager.id
  resource_id             = aws_api_gateway_resource.root.id
  http_method             = aws_api_gateway_method.get_root.http_method
  integration_http_method = "GET"
  type                    = "AWS"
  uri                     = aws_lambda_function.retrieve_password_from_user.invoke_arn
}

resource "aws_api_gateway_integration_response" "get_password_lambda" {
  http_method = aws_api_gateway_method.get_root.http_method
  resource_id = aws_api_gateway_resource.root.id
  rest_api_id = aws_api_gateway_rest_api.password_manager.id
  status_code = "200"

  depends_on = [
    aws_api_gateway_integration.post_password_lambda
  ]
}

resource "aws_api_gateway_method" "post_root" {
  authorization     = "NONE"
  http_method       = "POST"
  api_key_required  = true
  resource_id       = aws_api_gateway_resource.root.id
  rest_api_id       = aws_api_gateway_rest_api.password_manager.id
}

resource "aws_api_gateway_integration" "post_password_lambda" {
  rest_api_id             = aws_api_gateway_rest_api.password_manager.id
  resource_id             = aws_api_gateway_resource.root.id
  http_method             = aws_api_gateway_method.post_root.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.post_password.invoke_arn
}

resource "aws_api_gateway_integration_response" "post_password_lambda" {
  http_method = aws_api_gateway_method.post_root.http_method
  resource_id = aws_api_gateway_resource.root.id
  rest_api_id = aws_api_gateway_rest_api.password_manager.id
  status_code = "200"

  depends_on = [
    aws_api_gateway_integration.post_password_lambda
  ]
}

resource "aws_api_gateway_deployment" "password_manager" {
  rest_api_id = aws_api_gateway_rest_api.password_manager.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.root.id,
      aws_api_gateway_method.get_root.id,
      aws_api_gateway_method.post_root.id,
      aws_api_gateway_integration.get_password_lambda.id,
      aws_api_gateway_integration.post_password_lambda.id
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "password_manager" {
  deployment_id = aws_api_gateway_deployment.password_manager.id
  rest_api_id   = aws_api_gateway_rest_api.password_manager.id
  stage_name    = var.environment
}

resource "aws_api_gateway_api_key" "password_manager_key_a" {
  name = "${var.identifier}-${var.environment}-PasswordManager"
}

resource "aws_api_gateway_usage_plan" "password_manager_plan_a" {
  name = "${var.identifier}-${var.environment}-UsagePlan_A"
  api_stages {
    api_id = aws_api_gateway_rest_api.password_manager.id
    stage  = aws_api_gateway_stage.password_manager.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "key_a_plan_a" {
  key_id        = aws_api_gateway_api_key.password_manager_key_a.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.password_manager_plan_a.id
}


# /passwords
resource "aws_api_gateway_resource" "passwords" {
  rest_api_id = aws_api_gateway_rest_api.password_manager.id
  parent_id   = aws_api_gateway_rest_api.password_manager.root_resource_id
  path_part   = "passwords"
}

resource "aws_api_gateway_method" "get_passwords" {
  authorization     = "NONE"
  http_method       = "GET"
  api_key_required  = true
  resource_id       = aws_api_gateway_resource.passwords.id
  rest_api_id       = aws_api_gateway_rest_api.password_manager.id

  request_parameters = {
    "method.request.header.x-user-key" = true
    "method.request.querystring.customerId" = true
    "method.request.querystring.passwordKey" = false
  }
}

resource "aws_api_gateway_integration" "get_list_passwords_lambda" {
  rest_api_id             = aws_api_gateway_rest_api.password_manager.id
  resource_id             = aws_api_gateway_resource.passwords.id
  http_method             = aws_api_gateway_method.get_passwords.http_method
  integration_http_method = "GET"
  type                    = "AWS"
  uri                     = aws_lambda_function.list_password_key.invoke_arn

  passthrough_behavior = "WHEN_NO_TEMPLATES"

  request_templates = {
    "application/json" = <<EOF
{
    "method": "$context.httpMethod",
    "headers": {
        #foreach($param in $input.params().header.keySet())
        "$param": "$util.escapeJavaScript($input.params().header.get($param))"
        #if($foreach.hasNext),#end
        #end
    },
    "customerId": "$input.params('customerId')",
    "passwordKey": "$input.params('passwordKey')"
}
EOF
  }

}

resource "aws_api_gateway_integration_response" "get_list_passwords_lambda" {
  http_method = aws_api_gateway_method.get_passwords.http_method
  resource_id = aws_api_gateway_resource.passwords.id
  rest_api_id = aws_api_gateway_rest_api.password_manager.id
  status_code = "200"

  depends_on = [
    aws_api_gateway_integration.get_list_passwords_lambda
  ]
}