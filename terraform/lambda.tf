resource "aws_lambda_layer_version" "crypto_layer_hc" {
  layer_name = "${var.identifier}-${var.environment}-crypto_layer_hc"
  compatible_runtimes = [
    "python3.6",
    "python3.7",
    "python3.8"
  ]
  filename = "lambda_layers/crypto_layer_hc.zip"
  source_code_hash = filebase64sha256("./lambda_layers/crypto_layer_hc.zip")
}

data "archive_file" "retrieve_password" {
    type        = "zip"
    source_dir  = "../restApi/password/getPassword"
    output_path = "retrievePasswordFromUser.zip"
}

resource "aws_lambda_function" "retrieve_password_from_user" {
  filename      = "retrievePasswordFromUser.zip"
  function_name = "${var.identifier}-${var.environment}-RetrievePasswordFromUser"
  role          = aws_iam_role.password_manager_exec.arn
  handler       = "retrievePasswordFromUser.lambda_handler"

  source_code_hash = data.archive_file.retrieve_password.output_base64sha256

  runtime = "python3.7"

  layers = [aws_lambda_layer_version.crypto_layer_hc.arn]

  environment {
    variables = {
      identifier  = var.identifier,
      environment = var.environment
    }
  }
}

data "archive_file" "post_password" {
    type        = "zip"
    source_dir  = "../restApi/password/postPassword"
    output_path = "postPassword.zip"
}

resource "aws_lambda_function" "post_password" {
  filename      = "postPassword.zip"
  function_name = "${var.identifier}-${var.environment}-AddPasswordUser"
  role          = aws_iam_role.password_manager_exec.arn
  handler       = "postPassword.lambda_handler"

  source_code_hash = data.archive_file.post_password.output_base64sha256

  runtime = "python3.7"

  layers = [aws_lambda_layer_version.crypto_layer_hc.arn]

  environment {
    variables = {
      identifier  = var.identifier,
      environment = var.environment
    }
  }
}

data "archive_file" "list_password_key" {
    type        = "zip"
    source_dir  = "../restApi/password/getPasswords"
    output_path = "listPasswordKeyFromCustomer.zip"
}

resource "aws_lambda_function" "list_password_key" {
  filename      = "listPasswordKeyFromCustomer.zip"
  function_name = "${var.identifier}-${var.environment}-listPasswordKeyFromCustomer"
  role          = aws_iam_role.password_manager_exec.arn
  handler       = "listPasswordKeyFromCustomer.lambda_handler"

  source_code_hash = data.archive_file.list_password_key.output_base64sha256

  runtime = "python3.7"

  environment {
    variables = {
      identifier  = var.identifier,
      environment = var.environment
    }
  }
}