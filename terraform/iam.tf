resource "aws_iam_role" "password_manager_exec" {
  name = "${var.identifier}-${var.environment}-passwordManagerExecution"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "dynamodb-lambda-password-manager"

    policy = jsonencode({
      "Version": "2012-10-17",
      "Statement": [
          {
              "Action": [
                  "dynamodb:*",
              ],
              "Effect": "Allow",
              "Resource": [
                aws_dynamodb_table.password_manager.arn,
                aws_dynamodb_table.password_manager_user_key.arn,
                "${aws_dynamodb_table.password_manager.arn}/**/*"
              ]
          }
      ]
    })
  }
}